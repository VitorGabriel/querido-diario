import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://shrouded-shelf-01513.herokuapp.com/'
});

export default instance;