import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Login from '../screens/Login';
import Home from '../screens/Home';
import Details from '../screens/Details';
import Add from '../screens/Add';
import Settings from '../screens/Settings';

import TabBarIcon from '../components/TabBarIcon';
import AddButton from '../components/AddButton';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const AddBase = () => <View style={{ flex: 1, backgroundColor: "red" }} />

function Tabs() {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        headerShown: false,
        tabBarShowLabel: false,
        tabBarStyle: {
          height: 60, 
          borderTopWidth: 2, 
          borderColor: '#C4C4C4'
        },
        tabBarIcon: ({ focused, color }) => {
          let iconName;
          let bgColor;

          if (route.name === 'HomeStack') {
            iconName = 'home';
          } else if (route.name === 'Settings') {
            iconName = 'list';
          }

          if (focused) {
            bgColor = '#304FFE';
            color = '#fff';
          } else {
            bgColor = 'rgba(48, 79, 254, 0.1)';
            color = '#304FFE';
          }

          return <TabBarIcon iconName={iconName} bgColor={bgColor} color={color} />
        }
      })}
    >
      <Tab.Screen name='HomeStack' component={HomeStack} />
      <Tab.Screen 
        name='AddBase' 
        component={AddBase} 
        options={{
          tabBarIcon: () => {
            return <AddButton />
          }
        }}
        listeners={({ navigation }) => ({
          tabPress: event => {
            event.preventDefault();
            navigation.navigate('Add')
          }
        })} 
      />
      <Tab.Screen name='Settings' component={Settings} />
    </Tab.Navigator>
  );
}

function HomeStack() {
  return (
    <Stack.Navigator 
      initialRouteName='Home'
      screenOptions={{ headerShown: false }}
    >
      <Stack.Screen name='Home' component={Home} />
      <Stack.Screen name='Details' component={Details} />
      <Stack.Screen name='Add' component={Add} />
    </Stack.Navigator>
  );
}

export default function AppRoutes() {
  return (
    <NavigationContainer>
      <Stack.Navigator 
        initialRouteName='Login'
        screenOptions={{ headerShown: false }}
      >
        <Stack.Screen name='Login' component={Login} />
        <Stack.Screen name='Tabs' component={Tabs} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
