const activities = {
  sports: {
    text: 'esporte',
    iconName: 'soccer'
  },
  shopping: {
    text: 'compras',
    iconName: 'shopping'
  },
  rest: {
    text: 'descanso',
    iconName: 'bed'
  },
  party: {
    text: 'festa',
    iconName: 'party-popper'
  },
  movies: {
    text: 'filmes e séries',
    iconName: 'popcorn'
  },
  good_meal: {
    text: 'boa refeição',
    iconName: 'food-fork-drink'
  },
  games: {
    text: 'jogos',
    iconName: 'microsoft-xbox-controller'
  },
  date: {
    text: 'encontro',
    iconName: 'calendar-heart'
  },
  cooking: {
    text: 'cozinhar',
    iconName: 'cookie'
  }
}

export default activities;
