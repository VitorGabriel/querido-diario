const moods = {
  happy: {
    image: require('../assets/img/status/happy.png'),
    color: '#E24B4B',
    text: 'bem'
  },
  nervous: {
    image: require('../assets/img/status/nervous.png'),
    color: '#4B75E2',
    text: 'mal'
  },
  sad: {
    image: require('../assets/img/status/sad.png'),
    color: '#4BE263',
    text: 'triste'
  },
  confused: {
    image: require('../assets/img/status/confused.png'),
    color: 'purple',
    text: 'confuso'
  },
  sleeping: {
    image: require('../assets/img/status/sleeping.png'),
    color: 'pink',
    text: 'sono'
  }
}

export default moods;
