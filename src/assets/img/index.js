const images = {
  status: {
    bem: require('./status/happy.png'),
    mal: require('./status/nervous.png'),
    triste: require('./status/sad.png'),
    confuso: require('./status/confused.png'),
    sono: require('./status/sleeping.png')
  }
}

export default images;
