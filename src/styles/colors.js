const colors = {
  status: {
    bem: '#E24B4B',
    mal: '#4B75E2',
    triste: '#4BE263'
  }
}

export default colors;
