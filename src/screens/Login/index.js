import React from 'react';
import { 
  Image, 
  SafeAreaView,
  StatusBar,
  Text, 
  TouchableOpacity,
  View
} from 'react-native';

import styles from './styles';
import Input from '../../components/Input';

const Login = ({ navigation: {reset} }) => {
  function goToHomeScreen() {
    reset({
      index: 0,
      routes: [{name: 'Tabs'}]
    });
  }

  return (
    <SafeAreaView style={styles.background}>
      <StatusBar barStyle='light-content' backgroundColor='#5D21D0' />

      <View style={styles.logoContainer}>
        <Image source={require('../../assets/img/logo.png')} />
      </View>

      <View style={styles.container}>
        <Input placeholder='e-mail' iconName='mail' />
        <Input placeholder='senha' iconName='lock' secureTextEntry />

        <TouchableOpacity style={styles.loginBtn} onPress={goToHomeScreen}>
          <Text style={styles.loginTxt}>Entrar</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

export default Login;
