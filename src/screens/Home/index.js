import React, { useEffect, useState } from 'react';
import { FlatList, StatusBar, TouchableOpacity, View } from 'react-native';

import Empty from './Empty';
import RecordCard from '../../components/RecordCard';

import api from '../../services/api';

const Home = ({ navigation: {navigate} }) => {
  const [dailyEntries, setDailyEntries] = useState([]);

  useEffect(() => {
    api.get('/daily_entries?username=vitorgabriel')
    .then((response) => {
      setDailyEntries(response.data);
    })
    .catch((error) => console.error(error));
  }, [dailyEntries]);

  function renderItem({ item }) {
    return (
      <TouchableOpacity onPress={() => navigate('Details', {item})}>
        <RecordCard {...item} />
      </TouchableOpacity>
    );
  }

  {
    if (dailyEntries.length !== 0) {
      return (
        <>
          <StatusBar barStyle='dark-content' backgroundColor='#fff' />
    
          <View style={{ flex: 1, backgroundColor: '#E5E5E5' }}>
            <FlatList 
              contentContainerStyle={{ alignItems: 'center', paddingTop: 20, backgroundColor: '#E5E5E5' }}
              showsVerticalScrollIndicator={false}
              data={dailyEntries}
              renderItem={renderItem}
              keyExtractor={item => item.id}
            />
          </View>
        </>
      );
    } else {
      return <Empty />;
    }
  }
}

export default Home;
