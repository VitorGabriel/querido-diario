import React from 'react';
import { Image, StatusBar, Text, View } from 'react-native';

import styles from './styles';

const Empty = () => {
  return (
    <View style={styles.background}>
      <StatusBar barStyle='dark-content' backgroundColor='#E5E5E5' />

      <View style={styles.container}>
        <View style={styles.imgContainer}>
          <Image source={require('../../../assets/img/status/neutral.png')} />
        </View>
        <Text 
          style={styles.msg}>
          Você ainda não tem nenhum registro diário. Para começar, toque no ícone de adicionar.
        </Text>
      </View>
    </View>
  );
}

export default Empty;
