import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#E5E5E5',
    flex: 1,
    alignItems: 'center',
  },
  container: {
    width: '68%',
    alignItems: 'center',
  },
  imgContainer: {
    marginTop: 150,
    marginBottom: 40
  },
  msg: {
    color: '#ACACAC',
    fontSize: 18,
    textAlign: 'center',
    lineHeight: 22
  }
})

export default styles;
