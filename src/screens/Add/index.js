import React, { useEffect, useState } from 'react';
import { 
  Image, 
  Modal, 
  ScrollView, 
  Text, 
  TextInput,
  TouchableOpacity, 
  View 
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

import styles from './styles';
import api from '../../services/api'
import activitiesUtils from '../../utils/activities';
import Status from './Status';
import Activity from '../../components/Activity';

const Add = ({ navigation }) => {
  const [activities, setActivities] = useState([]);

  useEffect(() => {
    api.get('/activities').then((response) => {
      setActivities(response.data);
    })
    .catch((error) => console.error(error));
  }, []);

  const moods = ['happy','confused','sad','sleeping','nervous'];

  const [selectedMood, setSelectedMood] = useState('');
  function selectMood(mood) {
    if (mood === selectedMood) setSelectedMood('');
    else setSelectedMood(mood);
  }

  const [selectedActivities, setSelectedActivities] = useState([]); 
  const [count, setCount] = useState(0);
  function selectActivity(activity) {
    if (selectedActivities.includes(activity)) {
      setSelectedActivities(oldArray => [...oldArray].filter((item) => item !== activity));
      setCount(count - 1);
    } else if (count < 3) {
      setSelectedActivities(oldArray => [...oldArray, activity]);
      setCount(count + 1);
    }
  }

  const months = ['janeiro','fevereiro','março','abril','maio','junho','julho','agosto','setembro','outubro','novembro','dezembro'];

  const currentDate = new Date();
  let date = currentDate.getDate();
  if (date < 10) date = '0' + date;
  const month = months[currentDate.getMonth()];

  const hour = currentDate.getHours() - 3;
  let minutes = currentDate.getMinutes();
  if (minutes < 10) minutes = '0' + minutes;

  const [description, setDescription] = useState('');

  function addDailyEntries() {
    const daily_entry = {
      mood: selectedMood,
      activity_ids: selectedActivities,
      description: description,
      username: "vitorgabriel"
    }

    api.post('/daily_entries', {daily_entry})
    .then((response) => {
      navigation.navigate('Home');
      console.log(response.data)
    })
    .catch((error) => console.error(error));
  }
  
  return (
    <Modal animationType='slide' >
      <ScrollView showsVerticalScrollIndicator={false}>
        <TouchableOpacity style={styles.closeBtn} onPress={() => navigation.navigate('Home')}>
          <AntDesign name='close' size={24} color='#304FFE' />
        </TouchableOpacity>
        <View style={styles.container}>
          <Text style={styles.title}>Como você está?</Text>
          <View style={styles.timeContainer}>
            <Image
              style={styles.timeImg}
              source={require('../../assets/img/calendario.png')} 
            />
            <Text style={[styles.timeTxt, {marginRight: 20}]}>{`Hoje, ${date} de ${month}`}</Text>
            <Image 
              style={styles.timeImg}
              source={require('../../assets/img/relogio.png')} 
            />
            <Text style={styles.timeTxt}>{hour}:{minutes}</Text>
          </View>
          <View style={styles.statusContainer}>
            {
              moods.map((item) => {
                return (
                  <TouchableOpacity 
                    style={styles.status}
                    key={item}
                    onPress={() => selectMood(item)}
                  >
                    <Status name={item} selected={selectedMood} />
                  </TouchableOpacity>
                );
              })
            }
          </View>
          <View style={styles.activitiesContainer}>
            {
              activities.map((item) => {
                const selected = selectedActivities.includes(item.id);

                return (
                  <View style={styles.activity} key={item.id}>
                    <TouchableOpacity onPress={() => selectActivity(item.id)} >
                      <View style={[
                        {
                          borderWidth: 1,
                          borderRadius: 50,
                          padding: 8,
                          alignItems: 'center'
                        },
                        {backgroundColor: selected ? '#304FFE' : '#fff' }
                      ]}>
                        <Activity name={item.name} size={43} color={selected ? '#fff' : '#000'} />
                      </View>
                    </TouchableOpacity>
                    <Text style={styles.activityTxt}>{activitiesUtils[item.name].text}</Text>
                  </View>
                );
              })
            }
          </View>
          <TextInput 
            style={styles.txtArea}
            placeholder='Escreva aqui como foi seu dia' 
            multiline={true}
            numberOfLines={4}
            onChangeText={value => setDescription(value)}
          />
          <TouchableOpacity 
            style={styles.saveBtn}
            onPress={() => addDailyEntries()}
          >
            <Text style={styles.saveTxt}>Salvar</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </Modal>
  );
}

export default Add;
