import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  closeBtn: {
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    marginLeft: 20,
    marginTop: 20,
    backgroundColor: 'rgba(48, 79, 254, 0.1)'
  },
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: 20,
    color: '#000'
  },
  timeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10
  },
  timeImg: {
    marginRight: 5
  },
  timeTxt: {
    textTransform: 'uppercase',
    fontSize: 16,
    color: '#969696'
  },
  statusContainer: {
    flexDirection: 'row',
    marginTop: 20
  },
  status: {
    alignItems: 'center', 
    marginRight: 20
  },
  activitiesContainer: {
      marginTop: 20,
      width: '90%',
      borderWidth: 1,
      borderRadius: 10,
      paddingVertical: 20,
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'center',
      justifyContent: 'center'
  },
  activity: {
    alignItems: 'center',
    width: 100,
    margin: 10
  },
  activityTxt: {
    color: '#000'
  },
  txtArea: {
    width: '90%',
    alignItems: 'flex-start',
    padding: 14,
    marginTop: 20,
    borderWidth: 1,
    borderRadius: 10,
    textAlignVertical: 'top'
  },
  saveBtn: {
    width: '90%',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20,
    padding: 18,
    borderRadius: 6,
    backgroundColor: '#304FFE',
  },
  saveTxt: {
    textTransform: 'uppercase',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#fff'
  }
});

export default styles;
