import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

import moods from '../../../utils/moods';

const Status = ({ name, selected }) => {
  const isSelected = selected === name;

  return (
    <>
      <View style={[{padding: 5}, isSelected ? styles.selected : false]}>
        <Image 
          style={{ width: 43, height: 43 }}
          source={moods[name].image} 
        />
      </View>
      <Text 
        style={[styles.statusTxt, {color: isSelected ? '#C801FA' : '#969696'}]}>
        {moods[name].text}
      </Text>
    </>
  );
}

const styles = StyleSheet.create({
  statusTxt: {
    textTransform: 'uppercase',
    fontSize: 11,
    fontWeight: '700',
    marginTop: 5,
  },
  selected: {
    backgroundColor: '#304FFE',
    borderRadius: 50
  }
})

export default Status;
