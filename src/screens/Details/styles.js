import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  backBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 40,
    height: 40,
    borderRadius: 10,
    position: 'absolute',
    left: 20,
    top: 20,
    backgroundColor: 'rgba(48, 79, 254, 0.1)'
  },
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 40,
    backgroundColor: '#E5E5E5',
  },
  timeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10
  },
  timeTxt: {
    marginLeft: 10,
    fontSize: 16,
    textTransform: 'uppercase',
    color: '#969696'
  },
  statusContainer: {
    alignItems: 'center',
    marginVertical: 20
  },
  statusTxt: {
    textTransform: 'uppercase',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 10
  },
  activitiesContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    width: '95%',
    height: 140,
    marginBottom: 20,
    borderRadius: 20,
    backgroundColor: '#fff',
  },
  activity: {
    alignItems: 'center'
  },
  activityBackground: {
    padding: 10,
    marginBottom: 5,
    borderRadius: 50,
    backgroundColor: '#304FFE',
  },
  activityTxt: {
    textTransform: 'lowercase',
    fontWeight: '600',
    color: '#000'
  },
  descriptionContainer: {
    width: '95%',
    paddingVertical: 14,
    paddingHorizontal: 24,
    borderRadius: 20,
    backgroundColor: '#fff'
  }
});

export default styles;
