import React, { useEffect, useState } from 'react';
import { 
  Image, 
  SafeAreaView, 
  StatusBar,
  Text, 
  TouchableOpacity, 
  View 
} from 'react-native';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';

import styles from './styles';
import activitiesUtils from '../../utils/activities';
import moods from '../../utils/moods';
import Activity from '../../components/Activity';
import api from '../../services/api';

const Details = ({ navigation: {navigate}, route: {params} }) => {
  const {item} = params;

  const [description, setDescription] = useState('');
  const [mood, setMood] = useState('');
  const [dateDescription, setDateDescription] = useState('');
  const [hourDescription, setHourDescription] = useState('');
  const [activities, setActivities] = useState([]);
  const [image, setImage] = useState(null);
  const [color, setColor] = useState(null);
  const [moodTxt, setMoodTxt] = useState('');

  function hourFormat(dateTime) {
    const hour = dateTime.getHours();
    let minutes = dateTime.getMinutes();
    if (minutes < 10) minutes = '0' + minutes;

    setHourDescription(`${hour}:${minutes}`)
  }

  const months = ['janeiro','fevereiro','março','abril','maio','junho','julho','agosto','setembro','outubro','novembro','dezembro'];
  function dateFormat(dateTime) {
    const currentDate = new Date().getDate();
    let date = dateTime.getDate();
    if (date < 10) date = '0' + date;
    const month = months[dateTime.getMonth()];
    const diffDays = currentDate - date;

    let todayOrYesterday;
    if (diffDays < 1) {
      todayOrYesterday = 'hoje, ';
    } else if (diffDays < 2) {
      todayOrYesterday = 'ontem, ';
    } else {
      todayOrYesterday = '';
    }

    setDateDescription(`${todayOrYesterday}${date} de ${month}`)
  }

  useEffect(() => {
    api.get(`/daily_entries/${item.id}`)
    .then(({ data }) => {
      setDescription(data.description);
      setMood(data.mood);
      hourFormat(new Date(data.created_at));
      dateFormat(new Date(data.created_at));
      setActivities(data.activities);
      setImage(moods[data.mood].image);
      setColor(moods[data.mood].color);
      setMoodTxt(moods[data.mood].text);
    })
    .catch((error) => console.error(error));
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle='dark-content' backgroundColor='#fff' />

      <TouchableOpacity 
        style={styles.backBtn}
        onPress={() => navigate('Home')}
      >
        <AntDesignIcon name='left' size={24} color='#304FFE' />
      </TouchableOpacity>

      <View style={styles.timeContainer}>
        <Image source={require('../../assets/img/relogio.png')} />
        <Text style={styles.timeTxt}>{hourDescription}</Text>
      </View>
      
      <View style={styles.timeContainer}>
        <Image source={require('../../assets/img/calendario.png')} />
        <Text style={styles.timeTxt}>{dateDescription}</Text>
      </View>

      <View style={styles.statusContainer}>
        <Image 
          style={{ width: 65, height: 65 }} 
          source={image}
        />
        <Text style={[styles.statusTxt, {color: color}]}>
          {moodTxt}
        </Text>
      </View>

      <View style={styles.activitiesContainer}>
        {activities.map((item) => {
          return (
            <View style={styles.activity} key={item.id}>
              <View style={styles.activityBackground}>
                <Activity name={item.name} size={30} color='#fff' />
              </View>
              <Text style={styles.activityTxt}>{activitiesUtils[item.name].text}</Text>
            </View>
          );
        })}
      </View>

      <View style={styles.descriptionContainer}>
        <Text style={{ color: '#000' }}>
          {description}
        </Text>
      </View>
    </SafeAreaView>
  )
}

export default Details;
