import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    width: 380,
    padding: 20,
    marginBottom: 20,
    borderRadius: 20,
    backgroundColor: '#fff',
  },
  infoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  txtData: {
    textTransform: 'uppercase',
    color: '#ACACAC',
    fontSize: 16
  },
  statusContainer: {
    flexDirection: 'row',
    alignItems: 'baseline'
  },
  txtStatus: {
    textTransform: 'uppercase',
    fontSize: 22,
    fontWeight: 'bold',
  },
  txtHora: {
    color: '#ACACAC',
    fontSize: 14,
    marginLeft: 6
  },
  atividades: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 15
  },
  txtAtividade: {
    fontWeight: '600',
    color: '#000000',
    marginLeft: 6
  },
  descricao: {
    fontSize: 14,
    color: '#ACACAC'
  }
})

export default styles;
