import React from 'react';
import { Image, Text, View } from 'react-native';
import Fontisto from 'react-native-vector-icons/Fontisto';

import styles from './styles';
import moods from '../../utils/moods';
import activitiesUtils from '../../utils/activities';
import Activity from '../Activity';

const RecordCard = ({ mood, created_at, short_description, activities }) => {
  const months = ['janeiro','fevereiro','março','abril','maio','junho','julho','agosto','setembro','outubro','novembro','dezembro'];

  const currentDate = new Date();
  created_at = new Date(created_at);

  let date = created_at.getDate();
  if (date < 10) date = '0' + date;
  const month = months[created_at.getMonth()];

  const hour = created_at.getHours();
  let minutes = created_at.getMinutes();
  if (minutes < 10) minutes = '0' + minutes;

  const diffDays = currentDate.getDate() - created_at.getDate();

  let todayOrYesterday;
  if (diffDays < 1) {
    todayOrYesterday = 'hoje, ';
  } else if (diffDays < 2) {
    todayOrYesterday = 'ontem, ';
  } else {
    todayOrYesterday = '';
  }
  const dateDescription = todayOrYesterday + date + ' de ' + month;

  let image = moods[mood].image;
  let color = moods[mood].color;
  let moodTxt = moods[mood].text;

  return (
    <View style={styles.container} >
      <View style={styles.infoContainer}>
        <Image
          style={{ width: 57, height: 57 }} 
          source={image} 
        />
        <View style={{ marginLeft: 20 }}>
          <Text style={styles.txtData}>
            {dateDescription}
          </Text>
          <View style={styles.statusContainer}>
            <Text style={[styles.txtStatus, {color: color}]}>
              {moodTxt}
            </Text>
            <Text style={styles.txtHora}>
              {hour}:{minutes}
            </Text>
          </View>
        </View>
      </View>

      <View style={styles.atividades}>
        {activities.map((item, index) => {
          let ellipse;
          index !== (activities.length - 1) ? ellipse = true : ellipse = false;

          return (
            <View 
              style={{ flexDirection: 'row', alignItems: 'center' }} 
              key={item.id}
            >
              <Activity name={item.name} size={25} color='#000' />
              <Text style={styles.txtAtividade}>{activitiesUtils[item.name].text}</Text>
              {
                ellipse && (
                  <Fontisto 
                    style={{ marginHorizontal: 6 }}
                    name='ellipse' 
                    size={5} 
                    color='#000' 
                  />
                )
              }
            </View>
          );
        })}
      </View>
      <Text style={styles.descricao}>{short_description}</Text>
    </View>
  );
}

export default RecordCard;
