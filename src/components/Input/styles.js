import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginBottom: 10
  },
  input: {
    width: '100%',
    fontSize: 18,
    paddingHorizontal: 40,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: '#DADADA',
    backgroundColor: '#FFF'
  },
  icon: {
    position: 'absolute',
    top: 13,
    left: 10
  },
  secretIcon: {
    position: 'absolute',
    top: 13,
    right: 10
  }
});

export default styles;
