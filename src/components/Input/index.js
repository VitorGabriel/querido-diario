import React, { useState } from 'react';
import { TextInput, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

import styles from './styles';

const Input = ({ placeholder, iconName, secureTextEntry }) => {
  const [secure, setSecure] = useState(secureTextEntry);

  return (
    <View style={styles.container}>
      <TextInput 
        style={styles.input} 
        placeholder={placeholder} 
        secureTextEntry={secure}
      />
      
      <Icon 
        style={styles.icon} 
        name={iconName} 
        size={26} 
      />

      {
        secureTextEntry && (
          <TouchableOpacity onPress={() => setSecure(!secure)}>
            <Icon 
              style={styles.secretIcon} 
              name={secure ? 'eye' : 'eye-off'} 
              size={26} 
            />
          </TouchableOpacity>
        )
      }
    </View>
  );
}

export default Input;
