import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  background: {
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderRadius: 12,
  }
})

export default styles;