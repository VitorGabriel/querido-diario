import React from 'react';
import { View } from 'react-native';
import CommunityIcons from 'react-native-vector-icons/Ionicons';

import styles from './styles';

const TabBarIcon = ({ iconName, bgColor, color }) => {
  return (
    <View style={[styles.background, {backgroundColor: bgColor}]}>
      <CommunityIcons name={iconName} size={20} color={color} />
    </View>
  );
}

export default TabBarIcon;
