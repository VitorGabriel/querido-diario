import React from 'react';
import { View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

const AddButton = () => {
  return (
    <View>
      <Ionicons name='add-circle' size={60} color='#304FFE' />
    </View>
  );
}

export default AddButton;
