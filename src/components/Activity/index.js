import React from 'react';
import CommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import activities from '../../utils/activities';

const Activity = ({ name, size, color }) => {
  let iconName = activities[name].iconName;

  return (
    <CommunityIcons name={iconName} size={size} color={color} />
  );
}

export default Activity;
