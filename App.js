import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import AppRoutes from './src/routes/app.routes';

const Stack = createNativeStackNavigator();

function HomeStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name='HomeScreen' component={HomeScreen} />
      <Stack.Screen name='ProfileScreen' component={ProfileScreen} />
    </Stack.Navigator>
  )
}

function HomeScreen({ navigation }) {
  return (
    <> 
      <Text>Home Screen!</Text>
      <TouchableOpacity onPress={() => navigation.navigate('ProfileScreen')}>
        <Text>Transitar</Text>
      </TouchableOpacity>
    </>
  )
}

function SettingsScreen() {
  return (
    <Text>Settings Screen!</Text>
  )
}

function ProfileScreen() {
  return (
    <Text>Profile Screen</Text>
  )
}

const Tab = createBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={HomeStack} options={{headerShown: false}} />
      <Tab.Screen name="Settings" component={SettingsScreen} />
    </Tab.Navigator>
  )
}

const App = () => {
  return (
    <AppRoutes />
  );
};

export default App;
